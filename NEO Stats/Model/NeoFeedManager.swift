//
//  NeoFeedManager.swift
//  NEO Stats
//
//  Created by Naveen Natrajan on 28/06/22.
//

import Foundation
import Charts

protocol NeoFeedManagerDelegate {
    func didGetNeoFeed(neoFeed : NeoFeedJSON)
}

class NeoFeedManager {
    var delegate : NeoFeedManagerDelegate?
    var neoFeedData : NeoFeedJSON?

    
    func makePostCallNeoData(startDate : String , endDate : String) {
     
        let decoder = JSONDecoder()
        if let url = URL(string: "\(ConstantsUsedInProject.baseUrl)start_date=\(startDate)&end_date=\(endDate)&api_key=\(ConstantsUsedInProject.apiKey)")
        {
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
            let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
                guard error == nil && data != nil else {
                    print("error=\(String(describing: error))")
                    return
                }
                do {
                    let jsonResponse = try? decoder.decode(NeoFeedJSON.self, from: data!)
                    
                    DispatchQueue.main.async { [self] in
                        
                        if jsonResponse != nil {

                            delegate?.didGetNeoFeed(neoFeed: jsonResponse!)
                            neoFeedData = jsonResponse
                        }
                        else
                        {
                            print(String(data: data!, encoding: String.Encoding.utf8) as Any)

                        }
                    }
                }
            }
            task.resume()
        }
    }

    func calculateFastestAsteroid() -> NearEarthObject
    {
        var fastestAstreoid : NearEarthObject?
        if let safeData = neoFeedData
        {
            if safeData.nearEarthObjects?.count != 0
            {
                for i in safeData.nearEarthObjects!
                {
                    for j in i.value
                    {
                        if fastestAstreoid == nil
                        {
                            fastestAstreoid = j
                        }
                        let fastSpeed = Double(fastestAstreoid?.closeApproachData?[0].relativeVelocity?.kilometersPerHour ?? "0") ?? 0.0
                        let currSpeed = Double(j.closeApproachData?[0].relativeVelocity?.kilometersPerHour ?? "0") ?? 0.0
                        if currSpeed > fastSpeed
                        {
                            fastestAstreoid = j
                        }
                    }
                }
            }
        }
        return fastestAstreoid!
    }
    
    func calculateClosestAsteroid() -> NearEarthObject
    {
        var closestAstreoid : NearEarthObject?
        if let safeData = neoFeedData
        {
            if safeData.nearEarthObjects?.count != 0
            {
                for i in safeData.nearEarthObjects!
                {
                    for j in i.value
                    {
                        if closestAstreoid == nil
                        {
                            closestAstreoid = j
                        }
                        let closeAst = Double(closestAstreoid?.closeApproachData?[0].missDistance?.kilometers ?? "0") ?? 0.0
                        let currAst = Double(j.closeApproachData?[0].missDistance?.kilometers ?? "0") ?? 0.0
                        if currAst < closeAst
                        {
                            closestAstreoid = j
                        }
                    }
                }
            }
        }
        return closestAstreoid!
    }
    
    func calculateAverageSize() -> Double
    {
        var averageSize = 0.0
        var numberOfAsteroids = 0
        var totalSize = 0.0
        if let safeData = neoFeedData
        {
            if safeData.nearEarthObjects?.count != 0
            {
                for i in safeData.nearEarthObjects!
                {
                    for j in i.value
                    {
                        let currMin = j.estimatedDiameter?.kilometers?.estimatedDiameterMin ?? 0.0
                        let currMax = j.estimatedDiameter?.kilometers?.estimatedDiameterMax ?? 0.0
                        let currAverage = (currMax + currMin) / 2
                        totalSize = totalSize + currAverage
                        numberOfAsteroids = numberOfAsteroids + 1
                    }
                }
                averageSize = totalSize / Double(numberOfAsteroids)
            }
        }
        return averageSize
    }
    func createBarChartData() -> [BarChartDataSet]
    {
        var chartDataSets = [BarChartDataSet]()

        if let safeData = neoFeedData
        {
        if safeData.nearEarthObjects?.count != 0
        {
            for i in safeData.nearEarthObjects!
            {
                var set: BarChartDataSet! = nil
                let count = i.value.count
                set = BarChartDataSet(entries: [BarChartDataEntry(x: Double(count), y: Double(count))], label: i.key)
                let color : NSUIColor = .random
                set.colors = [color]
                chartDataSets.append(set)
                
            }
        }
        }
        return chartDataSets
        
    }
}


extension NSUIColor {
    static var random: UIColor {
        return UIColor(red: .random, green: .random, blue: .random, alpha: 1.0)
    }
}

extension CGFloat {
    static var random: CGFloat {
        return CGFloat(arc4random()) / CGFloat(UInt32.max)
    }
}
