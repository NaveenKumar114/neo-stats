//
//  ViewController.swift
//  NEO Stats
//
//  Created by Naveen Natrajan on 28/06/22.
//

import UIKit
import Charts
class ViewController: UIViewController, NeoFeedManagerDelegate {
    func didGetNeoFeed(neoFeed: NeoFeedJSON) {
        if neoFeedManager.neoFeedData != nil
        {
            let fastestAsteroid = neoFeedManager.calculateFastestAsteroid()
            fastestLabel.text = "ID: \(fastestAsteroid.id ?? "")\nSpeed: \(fastestAsteroid.closeApproachData?[0].relativeVelocity?.kilometersPerHour ?? "") km/h"
            let closestAsteroid = neoFeedManager.calculateClosestAsteroid()
            closestLabel.text = "ID: \(closestAsteroid.id ?? "")\nDistance: \(closestAsteroid.closeApproachData?[0].missDistance?.kilometers ?? "") km"
            let averageDistance = neoFeedManager.calculateAverageSize()
            averageLabel.text = "\(String(averageDistance)) km"
            let chartDataSet = neoFeedManager.createBarChartData()
            asteroidsChartView.data = BarChartData(dataSets: chartDataSet)
        }
    }
    
    @IBOutlet weak var asteroidsChartView: BarChartView!
    @IBOutlet weak var statsView: UIView!
    @IBOutlet weak var averageLabel: UILabel!
    @IBOutlet weak var closestLabel: UILabel!
    @IBOutlet weak var fastestLabel: UILabel!
    @IBOutlet weak var startDatePicker: UIDatePicker!
    @IBOutlet weak var endDatePicker: UIDatePicker!
    var neoFeedManager = NeoFeedManager()

    override func viewDidLoad() {
        super.viewDidLoad()
        neoFeedManager.delegate = self
        
       
        

    }

    @IBAction func submitButtonPressed(_ sender: Any) {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let startDate = formatter.string(from: startDatePicker.date)
        let endDate = formatter.string(from: endDatePicker.date)
        neoFeedManager.makePostCallNeoData(startDate: startDate, endDate: endDate)
      
    }

}

